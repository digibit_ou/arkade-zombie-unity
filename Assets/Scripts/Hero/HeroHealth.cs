﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeroHealth : MonoBehaviour {

    [SerializeField]
    GameManager gameManager;
    [SerializeField]
    GameObject panelAttacked, playerDie, pointCamDie;
    [SerializeField]
    int maxHealth;
    [SerializeField]
    Camera camMain, camWeapon;

    int currentHealth;

    public bool isDie;

    // Use this for initialization
    void Start () {
        panelAttacked.SetActive(false);
        isDie = false;
        currentHealth = maxHealth;
        panelAttacked.GetComponent<Image>().color = new Color
            (195f / 255f, 29 / 255f, 29 / 255f, (160 - currentHealth) / 255.0f);
        gameManager.SetTextHealth(currentHealth);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TakeDame(int _dam)
    {
        if (!isDie)
        {
            currentHealth -= _dam;
            currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
            gameManager.SetTextHealth(currentHealth);
            //Debug.Log(currentHealth);
            if (currentHealth <= 0)
            {
                panelAttacked.SetActive(true);
                isDie = true;
                camWeapon.gameObject.SetActive(false);
                camMain.transform.parent = null;
                camMain.transform.position = pointCamDie.transform.position;
                playerDie.SetActive(true);
                camMain.transform.LookAt(playerDie.transform);
                transform.GetComponent<Rigidbody>().isKinematic = true;
                gameManager.GameOver();
            }
            else
            {
                StartCoroutine(ShowAttacked());
            }
        }
    }

    IEnumerator ShowAttacked()
    {
        panelAttacked.SetActive(true);
        panelAttacked.GetComponent<Image>().color = new Color
            (195f / 255f, 29 / 255f, 29 / 255f, (160 - currentHealth) / 255.0f);
        yield return new WaitForSeconds(0.5f);
        if (!isDie)
        {
            panelAttacked.SetActive(false);
        }
    }

    public void AddHealth(int _heal)
    {
        currentHealth += _heal;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        gameManager.SetTextHealth(currentHealth);
    }

}
