﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class PlayerTurn : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{

     [SerializeField]
     Image objectCrosshair;
     [SerializeField]
     Camera cam;
     [SerializeField]
     float minRotateX, maxRotateX;

     Vector3 offset;
     float rotationY = 0.0f, rotationX = 0.0f, cameraSpeed = 20f;
     Quaternion ang;

     public static PlayerTurn instance;

     void Awake()
     {
          instance = this;
     }

     public void OnPointerDown(PointerEventData eventData)
     {
          Vector3 down = new Vector3(eventData.position.x, eventData.position.y, 0);
          offset = objectCrosshair.GetComponent<Image>().rectTransform.position - down;
     }

     public void OnDrag(PointerEventData eventData)
     {

          Vector3 drag = new Vector3(eventData.position.x, eventData.position.y, 0);
          Vector3 newPosition = offset + drag;

          rotationX = Mathf.Atan((objectCrosshair.GetComponent<Image>().rectTransform.position.y - newPosition.y) / 500) * Mathf.Rad2Deg;
          rotationY = cam.transform.localRotation.x + Mathf.Atan((newPosition.x - objectCrosshair.GetComponent<Image>().rectTransform.position.x) / 500) * Mathf.Rad2Deg;

          cam.transform.parent.Rotate(0, rotationY, 0);
          cam.transform.Rotate(rotationX, 0, 0);

          ang = cam.transform.localRotation;
          //Debug.Log("X: " + ang.x + " Y: " + ang.y + " Z: " + ang.z + " W: " + ang.w);
          ang.x = Mathf.Clamp(ang.x, minRotateX, maxRotateX);
          cam.transform.localRotation = ang;

          objectCrosshair.GetComponent<Image>().rectTransform.position = Vector3.Lerp(objectCrosshair.GetComponent<Image>().rectTransform.position, newPosition, cameraSpeed * Time.deltaTime);
     }


     public void Turn(float X, float Y)
     {
          float ClampMin = -45;
          float ClampMax = 45;
          X = Mathf.Clamp(X, ClampMin, ClampMax);

          ClampMin = -180;
          ClampMax = 180;
          Y = Mathf.Clamp(Y, ClampMin, ClampMax);

          cam.transform.parent.rotation = Quaternion.Euler(new Vector3(X, Y, 0));
     }

     public void OnPointerUp(PointerEventData eventData)
     {
          objectCrosshair.GetComponent<Image>().rectTransform.anchoredPosition = Vector3.zero;
     }

}
