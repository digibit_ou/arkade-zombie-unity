﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    [SerializeField]
    UIManager uiManager;
    [SerializeField]
    GameObject panelPlay, panelStart, panelGameOver, panelHelp, panelPause, loadingImage, panelShareSocial;
    
    public int numberGrenade;

    int score;
    public bool isHelp, isPause;

    public static bool isGiveBonus;

    public static GameManager instance;
    // Use this for initialization
    void Start () {
        instance = this;
		MasterCenter_IOS.Instance.ShowBanner ();
        if (panelPlay != null && panelStart != null && panelGameOver != null && panelHelp != null && panelPause != null)
        {
            panelPlay.SetActive(true);
            panelStart.SetActive(false);
            panelGameOver.SetActive(false);
            panelHelp.SetActive(false);
            panelPause.SetActive(false);
        }
        isHelp = false;
        isPause = false;
        isGiveBonus = true;
        score = -1;
        if (uiManager != null)
        {
            SetTextGrenade(numberGrenade);
            AddScore();
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddScore()
    {
        score++;
        uiManager.SetTextScore(score);
    }

    public void SetAmmo(string ammo)
    {
        uiManager.SetTextAmmo(ammo);
    }

    public void SetImageGun(int _indexOld, int _indexNew)
    {
        uiManager.SetImageGun(_indexOld, _indexNew);
    }

    public void AddGrenade(int num)
    {
        numberGrenade += num;
        SetTextGrenade(numberGrenade);
    }

    public void DecreGrenade()
    {
        numberGrenade--;
        SetTextGrenade(numberGrenade);
    }

    public void SetTextGrenade(int num)
    {
        uiManager.SetTextGrenade(num);
    }

    public void SetTextHealth(int heal)
    {
        uiManager.SetTextHealth(heal);
    }

    public void GameOver()
    {
		MasterCenter_IOS.Instance.ShowAdmobAds();
        panelPlay.SetActive(false);
        panelGameOver.SetActive(true);
    }

    public void ClickReplay()
    {
        SceneManager.LoadScene(1);
    }

    public void ClickHelp()
    {
        isHelp = !isHelp;
        panelHelp.SetActive(isHelp);
        panelPlay.SetActive(!isHelp);
        Time.timeScale = (isHelp) ? 0 : 1;
    }

    public void ClickPause()
    {
        isPause= !isPause;
		if (isPause) {
			MasterCenter_IOS.Instance.ShowAdmobAds();
		}
        panelPause.SetActive(isPause);
        panelPlay.SetActive(!isPause);
        Time.timeScale = (isPause) ? 0 : 1;
    }

    public void ClickMoreGames()
    {
        MasterCenter_IOS.Instance.ShowMoreGames();
    }

    public void ClickFb()
    {
        MasterCenter_IOS.Instance.ShareFacebook();
    }

    public void ClickTwitter()
    {
        MasterCenter_IOS.Instance.ShareTwitter();
    }

    public void ClickInstagram()
    {
        MasterCenter_IOS.Instance.ShareInstagram();
    }

    public void ClickLeader()
    {
        MasterCenter_IOS.Instance.Leaderboard_ShowLeaderboard();
    }

    public void ClickRate()
    {
        MasterCenter_IOS.Instance.RateApp();
    }

    public void ClickPlay()
    {
		
        loadingImage.SetActive(true);
		panelShareSocial.SetActive (false);
        SceneManager.LoadScene(1);
    }

    public void ClickMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ClickQuit()
    {
        Application.Quit();
    }

}
