﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    [SerializeField]
    Text textAmmo, textGrenade, textScore, textOverScore, textHealth;
    [SerializeField]
    Image[] gun;

	// Use this for initialization
	void Start () {
		MasterCenter_IOS.Instance.HideBanner ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetTextAmmo(string _text)
    {
        textAmmo.text = _text;
    }

    public void SetTextGrenade(int num)
    {
        textGrenade.text = num + "";
    }

    public void SetTextScore(int _score)
    {
        textScore.text = "Score: " + _score;
        textOverScore.text = "Your Score: " + _score;
    }

    public void SetImageGun(int _indexOld, int _indexNew)
    {
//        gun[_indexOld].gameObject.SetActive(false);
//        gun[_indexNew].gameObject.SetActive(true);
    }

    public void SetTextHealth(int heal)
    {
        textHealth.text = heal + "";
    }

}
