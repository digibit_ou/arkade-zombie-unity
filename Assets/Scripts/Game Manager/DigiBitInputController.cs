﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigiBitSDK;


public class DigiBitInputController : MonoBehaviour {


    bool ButtonUpLastState = false;
    bool ButtonDownLastState = false;
    bool ButtonLeftLastState = false;
    bool ButtonRightLastState = false;
    bool ButtonTriggerLastState = false;
    bool ButtonJoystickLastState = false;



    bool ButtonUpPressed = false;
    bool ButtonDownPressed = false;
    bool ButtonLeftPressed = false;
    bool ButtonRightPressed = false;
    bool ButtonTriggerPressed = false;
    bool ButtonJoystickPressed = false;


    bool ButtonUpReleased = false;
    bool ButtonDownReleased = false;
    bool ButtonLeftReleased = false;
    bool ButtonRightReleased = false;
    bool ButtonTriggerReleased = false;
    bool ButtonJoystickReleased = false;


	void Start () {
		
	}
	
	
    void Update()
    {
        UpdateButtonInput();
        WeaponControlUpdate();
        CharacterControlUpdate();
        AimControlUpdate();
    }


    void CheckButton(bool CurrentState, ref bool LastState, ref bool Pressed, ref bool Released)
    {
        if (CurrentState == true && LastState == false)
        {
            Pressed = true;
            Released = false;
        }
        else if (CurrentState == false && LastState == true)
        {
            Pressed = false;
            Released = true;
        }
        else
        {
            Pressed = false;
            Released = false;
        }

        LastState = CurrentState;

    }

    void UpdateButtonInput()
    {

        CheckButton(DigiBit.PrimaryData.ButtonUp, ref ButtonUpLastState, ref ButtonUpPressed, ref ButtonUpReleased);
        CheckButton(DigiBit.PrimaryData.ButtonDown, ref ButtonDownLastState, ref ButtonDownPressed, ref ButtonDownReleased);
        CheckButton(DigiBit.PrimaryData.ButtonLeft, ref ButtonLeftLastState, ref ButtonLeftPressed, ref ButtonLeftReleased);
        CheckButton(DigiBit.PrimaryData.ButtonRight, ref ButtonRightLastState, ref ButtonRightPressed, ref ButtonRightReleased);
        CheckButton(DigiBit.PrimaryData.ButtonTrigger, ref ButtonTriggerLastState, ref ButtonTriggerPressed, ref ButtonTriggerReleased);
        CheckButton(DigiBit.PrimaryData.ButtonJoystick, ref ButtonJoystickLastState, ref ButtonJoystickPressed, ref ButtonJoystickReleased);
        
     }

    void WeaponControlUpdate()
    {
        if (GameManager.instance.isPause)
            return;

        //Gun Fire
        if (DigiBit.PrimaryData.ButtonTrigger)
            HeroController.instance.ClickFire();
        else
            HeroController.instance.EndFire();

        //Throw Grenade
        if (ButtonRightPressed)
            HeroController.instance.ClickThrowGrenade();

        //Change Gun
        if (ButtonUpPressed)
            HeroController.instance.ClickNextGun();
        
        //Reload Gun
        if (ButtonDownPressed)
            HeroController.instance.ClickReloadGun();

        //Pause
        if (ButtonLeftPressed)
            GameManager.instance.ClickPause();
    }

    void AimControlUpdate()
    {
        if (GameManager.instance.isPause || !DigiBit.PrimaryConnected)
            return;

          PlayerTurn.instance.Turn(DigiBit.PrimaryData.EulerAngle.y, -1*DigiBit.PrimaryData.EulerAngle.z);
    }

    float Map(int value)
    {
        int OffsetValue = value - 1000;

        if (Mathf.Abs(OffsetValue) < 100)
            return 0.0f;

        float MappedValue = (OffsetValue +1000.0f) / (2000.0f) * (2.0f) -1.0f;

        if (OffsetValue > 1000)
            MappedValue = 1.0f;
        if (OffsetValue < -1000)
            MappedValue = -1.0f;


        return MappedValue;
    }

    void CharacterControlUpdate()
    {
        if (GameManager.instance.isPause || !DigiBit.PrimaryConnected)
            return;
        
        float h = Map(DigiBit.PrimaryData.JoyStickHorizontal);
        float v = Map(DigiBit.PrimaryData.JoyStickVertical);
        HeroController.instance.MoveCharacter(new Vector3(0, 0, 0));
    }
}
