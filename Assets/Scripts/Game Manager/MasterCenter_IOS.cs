using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class MasterCenter_IOS : MonoBehaviour {
	
	public static bool Init = false;
	private static MasterCenter_IOS master = null;
	public static MasterCenter_IOS Instance { get { return master; } }
	private InterstitialAd AdmobInterstitial;
	private BannerView bannerView;
	string admobFullAdsId = "ca-app-pub-8957221114927365/3498465931";
	string admobBannerID = "ca-app-pub-8957221114927365/2021732735";
	void Awake(){
		if (master != null) {
			Destroy (this.gameObject);
		} else {
			DontDestroyOnLoad (this.gameObject);
			master = this;
			Inializate ();
		}
	}

	void Inializate(){
#if UNITY_ANDROID && !UNITY_EDITOR
		if (PurchaseHandle.iRemoveAds == 1)
		return;
		AppLovin.InitializeSdk ();
		AppLovin.PreloadInterstitial();
		RequestAdmobInterstitial();
		RequestBanner ();
#endif
	}

	private void loadFriends()
	{
//		GameCenterManager.playerDataLoaded += friends =>
//		{
//			_friends = friends;
//			_hasFriends = _friends != null && _friends.Count > 0;
//		};
//		
//		Debug.Log( "player is authenticated so we are loading friends" );
//		GameCenterBinding.retrieveFriends( true, true );
	}
	
	public void ShowAppLovinInterstitial()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (AppLovin.HasPreloadedInterstitial ()) {
			// An ad is currently available, so show the interstitial.
			AppLovin.ShowInterstitial ();
		}
		#endif
	}

	public void ShowApplovinAds()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (PurchaseHandle.iRemoveAds == 1)
		return;	
		AppLovin.ShowInterstitial ();
		RequestAdmobInterstitial();
		#endif
	}

	public void ShowAdmobAds()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (PurchaseHandle.iRemoveAds == 1)
		return;
		if(AdmobInterstitial.IsLoaded())
			AdmobInterstitial.Show();
		else
			AppLovin.ShowInterstitial ();
		RequestAdmobInterstitial();
		AppLovin.PreloadInterstitial();
		#endif
	}

	private void RequestAdmobInterstitial()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		// Load an interstitial ad.
		AdmobInterstitial = new InterstitialAd(admobFullAdsId);
		AdmobInterstitial.LoadAd (createAdmobRequest ());
		#endif
	}
	
	// Returns an ad request with custom ad targeting.
	private AdRequest createAdmobRequest()
	{
		return new AdRequest.Builder()
			.AddTestDevice(AdRequest.TestDeviceSimulator)
				.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
				.AddKeyword("game")
				.SetGender(Gender.Male)
				.SetBirthday(new DateTime(1985, 1, 1))
				.TagForChildDirectedTreatment(false)
				.AddExtra("color_bg", "9B30FF")
				.Build();
	}

	private void RequestBanner ()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		//		string adUnitId = "ca-app-pub-8957221114927365/6549379538";
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView (admobBannerID, AdSize.Banner, AdPosition.Bottom);
		// Load a banner ad.
		AdRequest request = new AdRequest.Builder ().Build ();
		bannerView.LoadAd (request);
		#endif
	}

	public void ShowBanner ()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (PurchaseHandle.iRemoveAds == 1)
			return;
		if (bannerView != null) 
			bannerView.Show ();
		#endif
	}

	public void HideBanner ()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (bannerView != null) 
			bannerView.Hide ();
		#endif
	}
	
	public void ShowMoreGames()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		Application.OpenURL ("https://play.google.com/store/apps/developer?id=Android+Star+Games");
		#endif
	}

	public void Leaderboard_Publish(int score){
		#if UNITY_ANDROID && !UNITY_EDITOR
//		GameCenterBinding.reportScore(score,"grp.airfightercombat");
		#endif
	}
	
	public void Leaderboard_ShowLeaderboard(){
		#if UNITY_ANDROID && !UNITY_EDITOR
//		GameCenterBinding.showLeaderboardWithTimeScope(GameCenterLeaderboardTimeScope.AllTime);
		#endif
	}
	
	public void GameCenter(){
		#if UNITY_ANDROID && !UNITY_EDITOR
//		GameCenterBinding.showLeaderboardWithTimeScope(GameCenterLeaderboardTimeScope.AllTime);
		#endif
	}

	string appTitle = "Shooting Zombies 3D";
	string appURL = "https://play.google.com/store/apps/details?id=com.planetcaravalgames.shootingzombie3D";
	
	public void RateApp ()
	{
		Application.OpenURL (appURL);
	}

	public void ShareFacebook()
	{
		StartCoroutine(PostFBScreenshot());
	}

	private IEnumerator PostFBScreenshot() {
		
		
		yield return new WaitForEndOfFrame();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D( width, height, TextureFormat.RGB24, false );
		// Read screen contents into the texture
		tex.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
		tex.Apply();
		
		SPShareUtility.FacebookShare(appURL, tex);
		
		Destroy(tex);
		
	}

	public void ShareTwitter()
	{
		StartCoroutine(PostTWScreenshot());
	}


	private IEnumerator PostTWScreenshot() {
		
		
		yield return new WaitForEndOfFrame ();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D (width, height, TextureFormat.RGB24, false);
		// Read screen contents into the texture
		tex.ReadPixels (new Rect (0, 0, width, height), 0, 0);
		tex.Apply ();
		
		
		SPShareUtility.TwitterShare(appURL, tex);
		
		Destroy (tex);

	}

	public void ShareInstagram()
	{
		StartCoroutine(PostInstaScreenshot());
	}

	private IEnumerator PostInstaScreenshot() {
		
		
		yield return new WaitForEndOfFrame ();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D (width, height, TextureFormat.RGB24, false);
		// Read screen contents into the texture
		tex.ReadPixels (new Rect (0, 0, width, height), 0, 0);
		tex.Apply ();
		
		
		SPInstagram.instance.Share(tex, appURL);
		
		Destroy (tex);
		
	}
}
